package com.service;

import com.model.Cart;
import com.model.CartItem;

public interface CartItemService {
	public int i=0;

	void addCartItem(CartItem cartItem);
	void removeCartItem(String CartItemId);
	void removeAllCartItems(Cart cart);
}
